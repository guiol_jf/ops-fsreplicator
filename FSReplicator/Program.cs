﻿using System.Diagnostics;
using System.IO;
using System.Linq;
using FSReplicator.Config;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;
using Serilog.Exceptions;
using Serilog.Sinks.Network;

namespace FSReplicator
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var isService = !(Debugger.IsAttached || args.Contains("--console"));
            
            if (isService)
            {
                var pathToExe = Process.GetCurrentProcess().MainModule?.FileName;
                var pathToContentRoot = Path.GetDirectoryName(pathToExe);
                Directory.SetCurrentDirectory(pathToContentRoot);
            }

            var builder = CreateWebHostBuilder(
                args.Where(arg => arg != "--console").ToArray());
            var host = builder.Build();

            if (isService)
                host.RunAsCustomService();
            else
                host.DebugAsCustomService();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) => 
            WebHost.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureAppConfiguration((context, config) =>
                {
                    config
                        .SetBasePath(context.HostingEnvironment.ContentRootPath)
                        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                        .AddJsonFile($"appsettings.{context.HostingEnvironment.EnvironmentName}.json", optional: true)
                        .AddJsonFile("config.json", false, true)
                        .AddEnvironmentVariables();

                    var build = config.Build();
                    var logStashUrl = build.GetValue<string>("LogStashUrl");
                    var logStashPort = build.GetValue<int>("LogStashPort");
                    
                    Log.Logger = new LoggerConfiguration()
                        .Enrich.WithMachineName()
                        .Enrich.WithProcessId()
                        .Enrich.WithThreadId()
                        .Enrich.WithProperty("Application", "Corporate.FSReplicator")
                        .Enrich.WithExceptionDetails()
                        .MinimumLevel.Is(LogEventLevel.Information)
                        .WriteTo.UDPSink(logStashUrl, logStashPort)
                        .CreateLogger();
                })
                .UseStartup<Startup>();
    }
}