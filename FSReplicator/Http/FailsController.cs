using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FSReplicator.Config;
using FSReplicator.UseCase;
using Microsoft.AspNetCore.Mvc;

namespace FSReplicator.Http
{
    [Route("api/[controller]")]
    [ApiController]
    public class FailsController : Controller
    {
        private readonly IAppSettings _config;
        private readonly FindCopyFails _findCopyFails;
        private readonly CopyFile _copyFile;
        private readonly FeedCache _feedCache;

        public FailsController(IAppSettings config, FindCopyFails findCopyFails, CopyFile copyFile, FeedCache feedCache)
        {
            _config = config;
            _findCopyFails = findCopyFails;
            _copyFile = copyFile;
            _feedCache = feedCache;
        }

        [HttpGet("count")]
        public int FindTodayFailsCount()
        {
            return FindTodayFails().Count();
        }
        
        [HttpGet("count/{dateString}")]
        public int FindFailsCount([FromRoute]string dateString)
        {
            return FindTodayFails(dateString).Count();
        }
        
        [HttpGet("errors")]
        public IEnumerable<string> FindTodayFails()
        {
            return _findCopyFails.Execute(DateTime.Today.ToString("yyyy-MM-dd"));
        }
        
        [HttpGet("errors/{dateString}")]
        public IEnumerable<string> FindTodayFails([FromRoute]string dateString)
        {
            return _findCopyFails.Execute(dateString);
        }

        [HttpPost("recover")]
        public void RecoverFails()
        {
            var todayString = DateTime.Today.ToString("yyyy-MM-dd");
            _findCopyFails.Execute(todayString)
                .ToList()
                .ForEach(file => _copyFile.Execute(file, Path.Combine(_config.FolderToWatch, file)));
            _feedCache.Execute(todayString);
        } 
    }
}