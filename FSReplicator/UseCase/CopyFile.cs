using System;
using System.IO;
using System.Threading.Tasks;
using FSReplicator.Config;
using FSReplicator.Utils;
using Microsoft.Extensions.Logging;

namespace FSReplicator.UseCase
{
    public class CopyFile 
    {
        private readonly IAppSettings _config;
        private readonly ILogger _logger;
        private readonly FeedCache _feedCache;
        private readonly string _copyLogPrefix;

        public CopyFile(IAppSettings config, ILogger<CopyFile> logger, FeedCache feedCache)
        {
            _config = config;
            _logger = logger;
            _feedCache = feedCache;
            _copyLogPrefix = config.CopyLogPrefix;
        }

        public void Execute(string fileName, string fullPath)
        {
            try
            {
                var destinyPath = Path.Combine(_config.DestinyFolder, fileName);
                if (_config.CopyToTempBeforeMove)
                {
                    var tempPath = Path.Combine(_config.TempFolder, fileName);
                    Copy(fullPath, tempPath);
                    Move(tempPath, destinyPath);
                }
                else
                    Move(fullPath, destinyPath);
                LogFileCopy(fileName);
            } 
            catch (Exception)
            {
                _feedCache.Execute(DateTime.Today.ToString("yyyy-MM-dd"));
            }
        }
        
        private void Copy(string sourceFileName, string destFileName, int delay = 10)
        {
            try
            {
                using (var origFileStream = GetFileStream(sourceFileName, FileMode.Open))
                {
                    if (File.Exists(destFileName))
                        File.Delete(destFileName);
                    using (var destFileStream = GetFileStream(destFileName, FileMode.Create))
                        origFileStream.CopyTo(destFileStream);
                }
                _logger.LogInformation($"File {destFileName} successfully copied.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.GetBaseException().ToString());
                throw;
            }  
        }
        
        private void Move(string sourceFileName, string destFileName, int delay = 10)
        {
            try
            {
                if (File.Exists(destFileName))
                    File.Delete(destFileName);
                RetryHelper.RetryOnException(3, TimeSpan.FromSeconds(delay), File.Move, sourceFileName,
                    destFileName);
                _logger.LogInformation($"File {destFileName} successfully moved.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.GetBaseException().ToString());
                throw;
            }  
        }

        private FileStream GetFileStream(string path, FileMode fileMode, int delay = 5)
        {
            var attempts = 0;
            const int maxAttempts = 3;
            do
            {
                try
                {
                    attempts++;
                    return new FileStream(path, fileMode);
                }
                catch (Exception ex)
                {
                    _logger.LogInformation(ex.Message);
                    if (attempts > maxAttempts)
                        throw;
                    Task.Delay(TimeSpan.FromSeconds(delay)).Wait();
                }
            } while (true);
        }
        
        private void LogFileCopy(string copiedFile)
        {
            using (var log = File.AppendText(DateTime.Today.ToString("yyyy-MM-dd") + _copyLogPrefix))
                log.WriteLine(copiedFile);
        }
    }
}