using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FSReplicator.Config;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace FSReplicator.UseCase
{
    public class FeedCache
    {
        private readonly IAppSettings _config;
        private readonly ILogger _logger;
        private readonly IMemoryCache _cache;
        private readonly string _copyLogPrefix;

        public FeedCache(IAppSettings config, ILogger<FeedCache> logger, IMemoryCache cache)
        {
            _config = config;
            _logger = logger;
            _copyLogPrefix = config.CopyLogPrefix;
            _cache = cache;
        }

        public IEnumerable<string> Execute(string dateString)
        {
            var notCopiedFiles = FindNotCopiedFileNames(dateString).ToList();
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.MaxValue);
            _cache.Set(CacheKeys.Files, notCopiedFiles, cacheEntryOptions);
            _cache.Set(CacheKeys.CacheDate, DateTime.Today.ToString("yyyy-MM-dd"), cacheEntryOptions);
            _logger.LogInformation($"Cache successfully filled with {notCopiedFiles.Count} not copied files.");
            return notCopiedFiles;
        }

        private IEnumerable<string> FindNotCopiedFileNames(string dateString)
        {
            var localCopiedFiles = ReadLogFile(dateString + _copyLogPrefix);
            var originFolder = new DirectoryInfo(_config.FolderToWatch);
            var fileInfoArray = originFolder.GetFiles(_config.Filter);
            var fileNames = fileInfoArray
                .Where(file => file.CreationTime.Date.Equals(Convert.ToDateTime(dateString)))
                .Select(file => file.Name);
            return fileNames
                .Except(localCopiedFiles.Distinct());
        }
        
        private static IEnumerable<string> ReadLogFile(string logFile)
        {
            try
            {
                return File.ReadAllLines(logFile).ToList();
            }
            catch (FileNotFoundException)
            {
                return Enumerable.Empty<string>();
            }
        }
    }
}