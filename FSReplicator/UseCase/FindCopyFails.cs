using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using FSReplicator.Config;
using Microsoft.Extensions.Caching.Memory;

namespace FSReplicator.UseCase
{
    public class FindCopyFails
    {
        private readonly IMemoryCache _cache;
        private readonly FeedCache _feedCache;
        
        public FindCopyFails(IMemoryCache cache, FeedCache feedCache)
        {
            _cache = cache;
            _feedCache = feedCache;
        }
        
        public IEnumerable<string> Execute(string dateString)
        {
            ValidateDateString(dateString);
            if (!_cache.TryGetValue(CacheKeys.Files, out IEnumerable<string> files))
                return _feedCache.Execute(dateString);
            return _cache.Get<string>(CacheKeys.CacheDate) == dateString ? 
                files : _feedCache.Execute(dateString);
        }

        private static void ValidateDateString(string dateString)
        {
            const string dateRegex = @"(^(20)\d\d)-(0[1-9]|1[0-2])-((0|1)[0-9]|2[0-9]|3[0-1])$";
            if (string.IsNullOrEmpty(dateString) || !Regex.IsMatch(dateString, dateRegex))
                throw new ArgumentNullException(nameof(dateString));
        }
    }
}