using System;
using System.Threading.Tasks;

namespace FSReplicator.Utils
{
    public static class RetryHelper
    {
        public static void RetryOnException(int times, TimeSpan delay, Action<string, string> action, string arg1, string arg2)
        {
            var attempts = 0;
            do
            {
                try
                {
                    attempts++;
                    action(arg1, arg2);
                    break;
                }
                catch (Exception)
                {
                    if (attempts == times)
                        throw;
                    Task.Delay(delay).Wait();
                }
            } while (true);
        }
    }
}