﻿using System.ServiceProcess;
using FSReplicator.Service;
using Microsoft.AspNetCore.Hosting;

namespace FSReplicator.Config
{
    public static class WebHostServiceExtensions
    {
        public static void RunAsCustomService(this IWebHost host)
        {
            var webHostService = new CustomWebHostService(host);
            ServiceBase.Run(webHostService);
        }

        public static void DebugAsCustomService(this IWebHost host)
        {
            new CustomWebHostService(host).OnDebugging();
            host.Run();
        }
    }
}