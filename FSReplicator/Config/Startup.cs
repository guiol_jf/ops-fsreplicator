﻿using System.Diagnostics.CodeAnalysis;
using FSReplicator.UseCase;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;

namespace FSReplicator.Config
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddSingleton<CopyFile>()
                .AddSingleton<FindCopyFails>()
                .AddSingleton<FeedCache>()
                .AddSingleton(typeof(IAppSettings), typeof(AppSettings))
                .AddHealthChecks();
            services
                .AddLogging(builder => builder.AddSerilog())
                .AddMemoryCache()
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services
                .AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1",
                        new Info
                        {
                            Title = "FSReplicator",
                            Version = "v1",
                            Description = "Replicar arquivos MTA e SecurityClient",
                            Contact = new Contact
                            {
                                Name = "Guilherme de Oliveira e Lima",
                                Email = "guilherme.oliveira@xpi.com.br",
                                Url = "https://github.com/guilherme-lima"
                            }
                        });
                });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }
            app.UseSwagger();
            app.UseSwaggerUI(s =>
            {
                s.SwaggerEndpoint("/swagger/v1/swagger.json", "FSReplicator API V1");
            });
            app.UseHealthChecks("/health");
            app.UseMvc();
        }
    }
}