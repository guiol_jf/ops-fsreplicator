using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.Extensions.Configuration;

namespace FSReplicator.Config
{
    [ExcludeFromCodeCoverage]
    public class AppSettings : IAppSettings
    {
        private IConfiguration Configuration { get; }
        
        public AppSettings(IConfiguration configuration)
        {
            Configuration = configuration;
            ValidateSettings();
        }
        
        public string ServiceName => Configuration["ServiceName"];
        public string FolderToWatch => Configuration["FolderToWatch"];
        public string Filter => Configuration["Filter"];
        public string TempFolder
        {
            get
            {
                var tempFolder = Configuration["TempFolder"];
                return string.IsNullOrEmpty(tempFolder)
                    ? Environment.GetEnvironmentVariable("TEMP")
                    : tempFolder;
            }
        }

        public string DestinyFolder => Configuration["DestinyFolder"];
        public string CopyLogPrefix => Configuration["CopyLogPrefix"];
        public bool WatchEnabled => Convert.ToBoolean(Configuration["WatchEnabled"]);
        public bool CopyToTempBeforeMove => Convert.ToBoolean(Configuration["CopyToTempBeforeMove"]);


        private void ValidateSettings()
        {
            if (new List<string>(new[] {ServiceName, FolderToWatch, Filter, DestinyFolder, CopyLogPrefix}).Any(
                string.IsNullOrEmpty))
                throw new InvalidSettingsException("Missing mandatory settings. Check App.config.");
        }
    }

    [Serializable]
    internal class InvalidSettingsException : Exception
    {
        public InvalidSettingsException(string message) : base(message)
        {
        }
    }
}