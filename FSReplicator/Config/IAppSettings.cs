namespace FSReplicator.Config
{
    public interface IAppSettings
    {
        string ServiceName { get; }
        string FolderToWatch { get; }
        string Filter { get; }
        string TempFolder { get; }
        string DestinyFolder { get; }
        string CopyLogPrefix { get; }
        bool WatchEnabled { get; }
        bool CopyToTempBeforeMove { get; }
    }
}