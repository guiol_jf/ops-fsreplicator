using System.Diagnostics.CodeAnalysis;

namespace FSReplicator.Config
{
    [ExcludeFromCodeCoverage]
    public static class CacheKeys
    {
        public static string CacheDate => "_CacheDate";
        public static string Files => "_Files";
    }
}