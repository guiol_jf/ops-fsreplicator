﻿using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using FSReplicator.Config;
using FSReplicator.UseCase;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.WindowsServices;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace FSReplicator.Service
{
    [DesignerCategory("Code")]
    internal class CustomWebHostService : WebHostService
    {
        private FileSystemWatcher _watcher;
        private readonly CopyFile _copyFile;
        private readonly IAppSettings _config;
        private readonly ILogger _logger;

        public CustomWebHostService(IWebHost host) : base(host)
        {
            _copyFile = host.Services.GetRequiredService<CopyFile>();
            _config = host.Services.GetRequiredService<IAppSettings>();
            _logger = host.Services .GetRequiredService<ILogger<CustomWebHostService>>();
        }

        public void OnDebugging()
        {
            OnStarted();
        }

        protected override void OnStarted()
        {
            _logger.LogInformation("Service started. Loading watcher...");
            _watcher = new FileSystemWatcher
            {
                Path = _config.FolderToWatch,
                Filter = _config.Filter
            };
            _watcher.Created += OnEvent;
            _watcher.EnableRaisingEvents = _config.WatchEnabled;
            _logger.LogInformation("Watcher successfully loaded.");
        }

        protected override void OnStopping()
        {
            _logger.LogInformation("Stopping service.");
            _watcher.EnableRaisingEvents = false;
            _watcher.Dispose();
        }
        
        [MethodImpl(MethodImplOptions.Synchronized)]
        private void OnEvent(object sender, FileSystemEventArgs fileInfo)
        {
            _copyFile.Execute(fileInfo.Name, fileInfo.FullPath);
        }
    }
}